# Asignación de pines del ESP32.

| Pin  | Función   | Asignado | Reservado | Pull   Up/Down | Descripción                                 |
|------|-----------|----------|-----------|----------------|---------------------------------------------|
| 1    | GND1      | x        |           |                | Tierra                                      |
| 2    | 3V3       | x        |           |                | Alimentación 3.3V (3.3Vesp)                 |
| 3    | EN        | x        |           | up (delay)     | Enable                                      |
| 4    | Sensor_VP | x        |           |                | VoutBridge (tensión del puente   resistivo) |
| 5    | Sensor_VN |          |           |                |                                             |
| 6    | IO34      |          |           |                |                                             |
| 7    | IO35      |          |           |                |                                             |
| 8    | IO32      | x        |           | up (4.7K)      | S-SCLloxp3.3  Oximetro en 3.3V              |
| 9    | IO33      | x        |           | up (4.7K)      | S-SDAoxp3.3   Oximetro en 3.3V              |
| 10   | IO25      | x        |           |                | Elev GND - DAC Out                          |
| 11   | IO26      |          |           |                |                                             |
| 12   | IO27      | x        |           |                | RSToxp3.3 Oximeter reset 3.3V               |
| 13   | IO14      | x        |           |                | MFIO13.3   Oximter Linea IO auxiliar        |
| 14   | IO12      |          |           |                |                                             |
| 15   | GND2      | x        |           |                | Tierra                                      |
| 16   | IO13      |          |           |                |                                             |
| 17   | SD2       |          | x         |                |                                             |
| 18   | SD3       |          | x         |                |                                             |
| 19   | CMD       |          | x         |                |                                             |
| 20   | CLK       |          | x         |                |                                             |
| 21   | SD0       |          | x         |                |                                             |
| 22   | SD1       |          | x         |                |                                             |
| 23   | IO15      | x        |           |                | Touch   Button (interno)                    |
| 24   | IO2       | x        | x         | down (10K)     |                                             |
| 25   | IO0       | x        |           | up (10K)       | botón                                       |
| 26   | IO4       | x        |           | down (10K)     | En_Bridge Habilita el puente   resistivo    |
| 27   | IO16      | x        |           |                | LED3   Led Verde - ON en cero               |
| 28   | IO17      | x        |           |                | LED3 Led Rojo - ON en cero                  |
| 29   | IO5       | x        |           |                | LED3   Led Azul - ON en cero                |
| 30   | IO18      | x        |           |                | S-SDA2                                      |
| 31   | IO19      | x        |           |                | S-SCL2                                      |
| 32   | NC        |          | x         |                |                                             |
| 33   | IO21      | x        |           |                | Entemp   habilita temperatura               |
| 34   | RXD0      | x        | x         |                | RX Programacion Debug  Pcpu2_3              |
| 35   | TXD0      | x        | x         |                | TX   Programacion Debug  Pcpu2_2            |
| 36   | IO22      | x        |           |                | RX auxiliar                                 |
| 37   | IO23      | x        |           |                | TX   auxiliar                               |
| 38   | GND3      | x        |           |                | Tierra                                      |
| 39   | P_GND     | x        |           |                | Tierra                                      |
